---
dct:
  version: 0.1.0
  id: autonomy_CausalityOrientation_7fx7pw2p
  label: Autonomy Causality Orientation
  date: '2021-09-22'
  ancestry: ''
  retires: ''
  definition:
    definition: |
      Autonomy Causality Orientation is a latent tendency to interpret situations as being influenceable and controllable. Persons with this orientation tend to see the environment as a source of relevant information to make judgements on external events and as a source of relevant information to interpret the own internal experiences.

      This orientation is based on the person’s tendency to put emphasis on those aspects in the environment that are linked to the own interests and own values.  In this way, persons with this tendency have a higher self-regulation by creating opportunities themselves to engage in what they find interesting and important. Persons with this orientation are usually intrinsically motivated and the determination of the self is less dependent on extrinsic rewards. When extrinsic rewards are applied, there is a tendency to interpret them as an affirmation of their competence, rather than as controlling tool. Typically the autonomy orientation is also linked to the tendency to ignore those aspects in the environment that can be seen as controlling.

      This construct describes a tendency persons have in making interpretations about situations and in their reasoning about situations. It does not describe the outcome of this interpretations and reasoning, and it does not describe aspects of reality. In fact, the objective description of the reality should be fully separated of this construct, as the same reality can be interpreted as autonomous, controlling or impersonal, based on the person's orientation (see also constructs controlling_CausalityOrientation and impersonal_CausalityOrientation.
  measure_dev:
    instruction: "Autonomy Causality Orientation can be measured by using a questionnaire
      in which a respondent is asked to evaluate a series of hypothetical events (referred
      to as \"vignette\"), where each event describes either a social situation (for
      instance \"going to a party\") or an achievement oriented situation (for instance
      \"taking a test\").  For each vignette, present 3 different responses (referred
      to as \"item\"), for which each response represents a typical type of causality
      orientation (autonomous, controlled, impersonal). \nRespondents can be asked
      to score each item on a Likert-type of scale, for instance a scale with 7 options
      varying from \"very unlikely\" over \"moderately likely\" to \"very likely\".
      Higher scores on 1 item can then reflect the higher orientation of the respondent
      on this item.\n\nPreferably, minimal 17 vignettes (as used in the GCOS) are
      represented to the respondent, with a balanced introduction of vignettes related
      to social situations and vignettes related to achievement situations. The answers
      on the items can be combined in 3 subscales, which represent the autonomy, controlled
      and impersonal causality orientation.\n\nThe General Causality Orientation Scale
      (GCOS) is a self-report instrument that is regularly used to assess autonomy
      causality orientation. "
  measure_code:
    instruction: "Autonomy Causality Orientation is measured using a questionnaire.
      In the questionnaire, the questions or statements are referred to as \"items\"
      which are linked to either social situations or achievement related situations.
      \nItems can be coded as measuring Autonomy Causality Orientation if they measure
      the respondents evaluation of a situation as being influenceable and controllable
      by themselves.\nCheck if the evaluation contains elements that are related to
      the respondents own interests and values.\nThe questions or statements should
      include at least an element of personal interest and understanding that is measured."
  aspect_dev:
    instruction: "Conduct a qualitative study where participants are interviewed.
      These interviews are recorded and transcribed, afterwards coded, using the instructions
      for aspect coding below.\nIn this qualitative study, use questions similar to
      the following examples:\n\"Please tell us about a situation, where you worked
      hard to get a result but where you didn't achieve. \"\n\"Please tell us about
      a situation, where you had to enter a social event (a party, a cafe, ...) where
      you didn't know anybody. \"\nAsk questions related to either a social situation,
      either an achievement related situations and balance the 2 types of questions
      equally. Each question should allow the interviewer to observe if the participants
      is screening the situation for elements that are linked to his/her interest
      or understanding.\nIf necessary, for each question, ask the following subquestions:
      \"how did you interpret this situation\", \"describe in your own words how you
      came to this interpretation\". "
  aspect_code:
    instruction: "Expressions that demonstrate or imply that a situation is being
      evaluated as influenceable and controllable. More specific, in this evaluation
      of a situation it can be distinguished that it is based on the participants
      tendency to look for personal interests and understanding of the situation.\nAn
      example for the “personal interest” dimension: “When proposed a new job, I feel
      interested in the new challenge and also a bit nervous”. An example for the
      “understanding” dimension: “When failing a test, I usually want to know the
      reason why I did so poorly”. \n\nExpressions that relate to the evaluation of
      a situation as being controlled by factors outside one's own sphere of influence,
      should be coded as dct:ControlledCausalityOrientation.\n\nExpressions that relate
      to the evaluation of a situation as being out of control and fully subject to
      luck or fate, should be coded as dct:ImpersonalCausalityOrientation"
  rel: ~

---

