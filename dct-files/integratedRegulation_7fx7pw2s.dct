---
dct:
  version: 0.1.0
  id: integratedRegulation_7fx7pw2s
  label: Integrated Regulation
  date: '2021-09-22'
  ancestry: ''
  retires: ''
  definition:
    definition: "Integrated regulation is the disposition of persons to evaluate their
      actions and behaviors as being fully integrated with other aspects of the self.
      With this regulation, persons act out of a belief that the activity is fully
      in congruence with other aspects of the self and behavior occurs when it is
      thought to be fully in line with one's personal set of beliefs and values. \n\nIntegrated
      regulation is a subconstruct of the more broader construct of Self-Regulation
      (see DCT SelfRegulation). \n\n"
  measure_dev:
    instruction: "Integrated regulation can be measured by using a questionnaire in
      which a respondent is asked to evaluate a series of statements that concern
      the regulation of a behavior. These behaviors relate to a certain domain of
      life,  (for instance exercising, studying, practicing). For each domain, a different
      set of statements can be made: regulatory styles are expected not to be stable
      over different events, therefore different domain related questionnaires are
      relevant. Statements in the questionnaire typically concern the reason why a
      respondent has a certain type of behavior. One statement can represent one item.
      Respondents can be asked to score each item on a Likert-type of scale. Higher
      scores on 1 item can then reflect the higher orientation of the respondent on
      this item.\n\n\n\nThe Revised Self Determination Scale for Exercise (E-SDSE)
      is a self-report instrument that can be used to assess integrated regulation
      . "
  measure_code:
    instruction: "Integrated Regulation is measured using a questionnaire. In the
      questionnaire, the statements are referred to as \"items\" which are linked
      to a domain specific type of behavior.  \n\nItems can be coded as measuring
      Integrated Regulation if they measure the respondents evaluation of a behavior
      as being fully assimilated and consistent with their values and beliefs.\n\nCheck
      if the evaluation contains elements that are related to the respondents evaluation
      of the activity as being fully consistent to the own value and beliefs.\n"
  aspect_dev:
    instruction: "Conduct a qualitative study where participants are interviewed.
      These interviews are recorded and transcribed, afterwards coded, using the instructions
      for aspect coding below.\nIn this qualitative study, use questions similar to
      the following examples:\n\"Please tell us about the reason why you exercise.
      \"\n\"Please tell us about the reason why you make your homework.\"\nAsk questions
      related to a domain in life where the participant can set certain behaviors
      that can be integrated in other aspects of the self, such as practicing or exercising.
      \nAvoid asking questions related to a domain in life where the participant needs
      to set certain behaviors that are not usually pleasant or rewarding in itself
      such as health behaviors, tasks, duties.\nFor each question, ask the following
      subquestions: \"what was your interpretation of the situation\", \"please describe
      the reasoning how you came to this interpretation\". "
  aspect_code:
    instruction: "Expressions that demonstrate or imply that a behavior is fully integrated
      in other aspects of the person's values, needs and goals. \n\nAn example for
      an expression that can be coded as \"integrated regulation”: \"I exercise because
      doing sport and being myself are inseparable\". \"Practicing music is an essential
      part of my self\". \n\nExpressions that relate to the evaluation of behavior
      as being regulated by an external punishment or reward, should be coded as dct:ExternalRegulation.\n\nExpressions
      that relate to the evaluation of behavior as being regulated by an internal
      punishment or reward, should be coded as dct:IntrojectedRegulation.\n\nExpressions
      that demonstrate or imply that a behavior is evaluated as being important in
      relation to the own values, needs and goals            should be coded as dct:IdentifiedRegulation.\n\n"
  rel: ~

---

