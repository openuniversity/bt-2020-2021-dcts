---
dct:
  version: 0.1.0
  id: susceptibilityToControl_AutonomousFunctioning_7fx7pw2r
  label: Susceptibility to Control
  date: '2021-09-22'
  ancestry: ''
  retires: ''
  definition:
    definition: "Susceptibility to Control is the disposition of being more or less
      influenced by inner or outer control.  \n\nSusceptibility to Control is a subconstruct
      of the more broader construct of Autonomous Functioning (see DCT AutonomousFunctioning).
      \n\nSusceptibility to Control concerns the absence or presence of internal and
      external pressure that drives the behavior."
  measure_dev:
    instruction: |-
      Susceptibility to control can be measured by using a questionnaire in which a respondent is asked to score a serie of statements  on their general experiences. Each statement represents one item in the questionnaire, respondents can be asked to score each item on a Likert-type of scale. Items that reflect this construct capture the tendency of a respondent to reflect on inner and outer events in a non-judgemental manner, in order to get insight in oneself or in one's experiences.

      Susceptibility to control is a subscale of the construct "Autonomous Functioning". When measuring Autonomous Functioning the following constructs should be measured: statements that have as a goal to assess authorship/self-congruence, statements to assess interest-taking and statements to assess susceptibility to control.

      Preferably, minimal 15 items are represented to the respondent, with a balanced introduction of items related the 3 subconstructs of Autonomous Functioning. The answers on the items can be combined in 3 subscales, which represent Authorship/Self-Congruence, Susceptibility to control and Interest-taking.

      The Index of Autonomous Functioning (IAF) is a self-report instrument that is regularly used to assess susceptibility to control.
  measure_code:
    instruction: |-
      Susceptibility to control is measured using a questionnaire. In the questionnaire, the questions or statements are referred to as "items" which are linked to the disposition of perceiving internal or external pressure as a motivator for behavior.


      Check if the evaluation contains elements that are related to the respondents disposition to experience internal or external pressure related to his/her behavior.
  aspect_dev:
    instruction: "Conduct a qualitative study where participants are interviewed.
      These interviews are recorded and transcribed, afterwards coded, using the instructions
      for aspect coding below.\nIn this qualitative study, ask participants to comment
      on statements similar to the following examples:\n\"I do things in order to
      avoid feeling ashamed. \"\n\"I do certain things so that others will like me\".\nEach
      statement should relate to at least one of the following dimensions: feeling
      internal pressure as a motivation of behavior, feeling external pressure as
      a motivation for behavior.\nIf necessary, for each statement, ask the following
      subquestions: \"how did you interpret the statement\", \"describe in your own
      words how you came to this interpretation\". "
  aspect_code:
    instruction: "Expressions that demonstrate or imply that one experiences inner
      or outer pressure as a motivation for behavior. More specific, one of the following
      dimensions can be distinguished in this experience: experience that your behavior
      is motivated by internal or external pressure .\nAn example for the “experiencing
      internal pressure” dimension: “I do things to avoid feeling ashamed”. An example
      for the “experience of external pressure\" dimension: “I do things so that others
      will like me”. \n\nExpressions that relate to the demonstration of the tendency
      to interpret situations as being the author of behavior should be coded as dct:Authorship/self-congruence..\n\nExpressions
      that relate to the demonstration of the tendency to reflect on inner and outer
      events be coded as dct:InterestTaking."
  rel: ~

---

